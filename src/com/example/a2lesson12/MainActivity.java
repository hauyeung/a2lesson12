package com.example.a2lesson12;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

	private ListView videolistview;
	private ArrayList<String> videolist;
	private ArrayAdapter<String> videoadapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		videolistview = (ListView) findViewById(R.id.video_list);
		videolist = new ArrayList<String>();
		videolist.add("Coffee Cup");
		videolist.add("Angel of Peace");
		videolist.add("Window Blinds");
		videoadapter = new ArrayAdapter<String>(MainActivity.this, R.layout.videotext, videolist);
		videolistview.setAdapter(videoadapter);
		videolistview.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				Intent vi = new Intent(MainActivity.this, VideoActivity.class);
				vi.putExtra("position", position);
				System.out.println(position);
				startActivity(vi);
			}
			
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
