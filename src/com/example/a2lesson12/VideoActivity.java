package com.example.a2lesson12;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class VideoActivity extends Activity {

  final private int[] VIDEO_IDS = new int[] {R.raw.coffee_cup, R.raw.angel_of_peace, R.raw.window_blinds};
  
  private int mVideoIndex = 0;
  private int mLastProgress = 0;
  private boolean mWasPlaying = false;
  
  private TextView mSizeText;
  private CheckBox mLoopCheckBox;
  private VideoView mVideoView;
  private static Intent i;
  
  private OnClickListener mNextOnClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      mVideoIndex = (mVideoIndex + 1) % VIDEO_IDS.length;
      loadVideo(mVideoIndex);
    }
  };
  
  private OnClickListener mPreviousOnClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      mVideoIndex = mVideoIndex == 0? VIDEO_IDS.length - 1 : mVideoIndex - 1;
      loadVideo(mVideoIndex);
    }
  };
  
  private OnCompletionListener mOnCompletionListener = new OnCompletionListener() {
    @Override
    public void onCompletion(MediaPlayer mp) {
      if (mLoopCheckBox.isChecked()) {
        mVideoView.start();
      }
    }
  };

  private OnPreparedListener mOnPreparedListener = new OnPreparedListener() {
    @Override
    public void onPrepared(MediaPlayer mp) {
      mSizeText.setText(String.format(getResources().getString(R.string.size_text_format), mp.getVideoWidth(), mp.getVideoHeight()));
    }
  };
    
  private void loadVideo(int index) {
    mVideoView.setVideoURI(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + VIDEO_IDS[index]));
    mVideoView.start();
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      i = getIntent();
      setContentView(R.layout.activity_video);
      mLoopCheckBox = (CheckBox)findViewById(R.id.loop_checkbox);
      mSizeText = (TextView)findViewById(R.id.video_size_text);
      findViewById(R.id.next_button).setOnClickListener(mNextOnClickListener);
      findViewById(R.id.previous_button).setOnClickListener(mPreviousOnClickListener);
      
      mVideoView = (VideoView)findViewById(R.id.video_view);
      MediaController controller = new MediaController(this);
      mVideoView.setMediaController(controller);
      mVideoView.setOnPreparedListener(mOnPreparedListener);
      mVideoView.setOnCompletionListener(mOnCompletionListener);
      
      mVideoIndex  = i.getIntExtra("position", 0);      
      loadVideo(mVideoIndex);
      
      
      Button back = (Button) findViewById(R.id.back_button);
      back.setOnClickListener(new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent m = new Intent(VideoActivity.this, MainActivity.class);
			startActivity(m);
		}
    	  
      });
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (mVideoView.isPlaying()) {
      mVideoView.pause();
      mWasPlaying = true;
    } else {
      mWasPlaying = false;
    }
    mLastProgress = mVideoView.getCurrentPosition();
  }
  
  @Override
  protected void onResume() {
    super.onResume();
    mVideoView.setVideoURI(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + VIDEO_IDS[mVideoIndex]));
    mVideoView.seekTo(mLastProgress);
    if (mWasPlaying)
      mVideoView.start();
  }
  
  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putInt("videoIndex", mVideoIndex);
    outState.putInt("progress", mVideoView.getCurrentPosition());
    outState.putBoolean("wasPlaying", mVideoView.isPlaying());
  }
  
  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    mVideoIndex = savedInstanceState.getInt("videoIndex");
    mLastProgress = savedInstanceState.getInt("progress");
    mWasPlaying = savedInstanceState.getBoolean("wasPlaying");
  }
}
